#====================================================================================
# makeESPArduino
#
# A makefile for ESP8286 Arduino projects.
# Edit the contents of this file to suit your project
# or just include it and override the applicable macros.
#
# License: GPL 2.1
# General and full license information is available at:
#    https://github.com/plerup/makeEspArduino
#
# Copyright (c) 2016 Peter Lerup. All rights reserved.
#
#====================================================================================

#====================================================================================
# User editable area
#====================================================================================

#=== Project specific definitions: sketch and list of needed libraries
SKETCH ?= ESPEasy.ino
LIBS ?= $(ESP_LIBS)/Wire \
        $(ESP_LIBS)/ESP8266WiFi \
        $(ESP_LIBS)/ESP8266mDNS \
        $(ESP_LIBS)/ESP8266WebServer \
        $(ESP_LIBS)/DNSServer \
        $(ESP_LIBS)/Servo \
        $(ESP_LIBS)/ESP8266HTTPUpdateServer \
        ./Libraries/pubsubclient \
        ./Libraries/ArduinoJson \
        ./Libraries/IRremoteESP8266 \
        ./Libraries/LiquidCrystal_I2C
        
# Esp8266 Arduino git location
# ESP_ROOT ?= $(HOME)/esp8266/
# Output directory
BUILD_ROOT ?= ./build/$(MAIN_NAME)

# Board definitions
FLASH_SIZE ?= 4M
FLASH_MODE ?= dio
FLASH_SPEED ?= 40
FLASH_LAYOUT ?= eagle.flash.4m.ld

# Upload parameters
UPLOAD_SPEED ?= 230400
UPLOAD_PORT ?= /dev/ttyUSB0
UPLOAD_VERB ?= -v

include $(ESP_MAKEFILE)/makeEspArduino.mk
